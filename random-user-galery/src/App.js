import './App.css';

import RandomUserGaleryContainer from './components/RandomUsersGalery/container';

function App(props) {
  return (
    <div
    className="mx-auto container"
    >
      <RandomUserGaleryContainer />
    </div>
  );
}

export default App;
