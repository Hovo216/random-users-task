import React, { useEffect, useState } from "react";
import RandomUsersGalery from "./view";

import { receive_users } from "../../redux/action";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";

import { setIsFetching } from "../../redux/action";

const RandomUserGaleryContainer = (props) => {
  const [modal, setModal] = useState(false);
  const [selectedUser, setSelectedUser] = useState({});

  useEffect(() => {
    props.receive_users(
      props.location.search ? props.location.search : "?page=1"
    );
  }, [props.location.search]);

  const toggle = () => {
    if (modal === true) {
      setSelectedUser({});
    }
    setModal(!modal);
  };

  return (
    <RandomUsersGalery
      users={props.users}
      setSelectedPage={props.setSelectedPage}
      modal={modal}
      toggle={toggle}
      selectedUser={selectedUser}
      setSelectedUser={setSelectedUser}
      isFetching={props.is_fetching}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    users: state.users,
    is_fetching: state.is_fetching,
  };
};

export default compose(
  connect(mapStateToProps, { receive_users, setIsFetching }),
  withRouter
)(RandomUserGaleryContainer);
