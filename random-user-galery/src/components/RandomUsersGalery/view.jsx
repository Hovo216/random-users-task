import React from "react";
import Styles from "./styles.module.css";

import UserItem from "../UserItem/view";
import Modal from "../Modal/modal";
import Pagination from "../Pagination/pagination";
import preloader from "../../static/preloader.svg";

const RandomUsersGalery = (props) => {
  const users_list = props.users.map((user) => {
    return (
      <UserItem
        user={user}
        key={user.login.uuid}
        toggle={props.toggle}
        setSelectedUser={props.setSelectedUser}
      />
    );
  });
  console.log(props.isFetching);
  return (
    <div className={Styles.container}>
      <div
        className={`${Styles.header} d-flex justify-content-center align-items-center`}
      >
        <span>Contacts</span>
      </div>
      {props.isFetching ? (
        <div
          className={`${Styles.preloader} d-flex align-items-center justify-content-center`}
        >
          <img src={preloader} alt="preloader" />
        </div>
      ) : (
        users_list
      )}
      {!props.isFetching && (
        <div className={Styles.pagination}>
          <Pagination />
        </div>
      )}
      <Modal
        modal={props.modal}
        toggle={props.toggle}
        selectedUser={props.selectedUser}
      />
    </div>
  );
};

export default RandomUsersGalery;
