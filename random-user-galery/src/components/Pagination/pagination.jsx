import React from "react";
import { Link, withRouter } from "react-router-dom";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";

const Example = (props) => {
  const number = props.location.search.split("=")[1];
  return (
    <Pagination aria-label="Page navigation example">
      <PaginationItem>
        <Link to="/users?page=1" onClick={() => window.scrollTo(0, 0)}>
          <PaginationLink first />
        </Link>
      </PaginationItem>
      <PaginationItem>
        <Link
          to={`/users?page=${number == 1 ? 1 : number - 1}`}
          onClick={() => window.scrollTo(0, 0)}
        >
          <PaginationLink previous />
        </Link>
      </PaginationItem>
      <PaginationItem>
        <Link to="/users?page=1" onClick={() => window.scrollTo(0, 0)}>
          <PaginationLink>1</PaginationLink>
        </Link>
      </PaginationItem>
      <PaginationItem>
        <Link to="/users?page=2" onClick={() => window.scrollTo(0, 0)}>
          <PaginationLink>2</PaginationLink>
        </Link>
      </PaginationItem>
      <PaginationItem>
        <Link to="/users?page=3" onClick={() => window.scrollTo(0, 0)}>
          <PaginationLink>3</PaginationLink>
        </Link>
      </PaginationItem>
      <PaginationItem>
        <Link to="/users?page=4" onClick={() => window.scrollTo(0, 0)}>
          <PaginationLink>4</PaginationLink>
        </Link>
      </PaginationItem>
      <PaginationItem>
        <Link to="/users?page=5" onClick={() => window.scrollTo(0, 0)}>
          <PaginationLink>5</PaginationLink>
        </Link>
      </PaginationItem>
      <PaginationItem>
        <Link
          to={`/users?page=${number == 5 ? 5 : +number + 1}`}
          onClick={() => window.scrollTo(0, 0)}
        >
          <PaginationLink next />
        </Link>
      </PaginationItem>
      <Link to="/users?page=5" onClick={() => window.scrollTo(0, 0)}>
        <PaginationLink last />
      </Link>
    </Pagination>
  );
};

export default withRouter(Example);
