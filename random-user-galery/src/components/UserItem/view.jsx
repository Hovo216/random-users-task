import React from "react";

import Styles from "./styles.module.css";

const UserItem = (props) => {
  const userClick = (user) => {
    props.setSelectedUser(user);
    props.toggle();
  };
  const date = new Date(props.user.dob.date);
  const birth_date = `${date.getDate()}-${
    date.getMonth() + 1
  }-${date.getFullYear()}`;
  return (
    <div
      className={`${Styles.userItem} row`}
      onClick={() => userClick(props.user)}
    >
      <div className={`${Styles.userIcon} col-md-3`}>
        <img src={props.user.picture.large} />
      </div>
      <div className={`${Styles.userContact} col-md-9`}>
        <h3>
          {props.user.name.title} {props.user.name.first} {props.user.name.last}
        </h3>
        <div className={Styles.infoItems}>
          <div className={Styles.labels}>
            <p>Nationality: </p>
            <p>Gender: </p>
            <p>Day of Birth: </p>
            <p>Email: </p>
            <p>Phone: </p>
          </div>
          <div className={Styles.info}>
            <p id="nat">{props.user.nat}</p>
            <p id="gender">{props.user.gender}</p>
            <p id="dob">
              {birth_date}, {props.user.dob.age} years
            </p>
            <p id="email">{props.user.email}</p>
            <p id="phone">
              <img src="/contact_phone_black_24dp.svg" alt="phone" />{" "}
              {props.user.phone} / {props.user.cell}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserItem;
