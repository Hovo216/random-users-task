import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Styles from "./styles.module.css";

const UserModal = (props) => {
  const { className } = props;

  const callUser = () => alert(`Calling... ${props.selectedUser.cell}`);

  const date = new Date(props.selectedUser.dob?.date);
  const birth_date = `${date.getDate()}-${
    date.getMonth() + 1
  }-${date.getFullYear()}`;

  return (
    <div>
      <Modal isOpen={props.modal} toggle={props.toggle} className={className}>
        <ModalHeader toggle={props.toggle}>
          {props.selectedUser.name?.title} {props.selectedUser.name?.first}{" "}
          {props.selectedUser.name?.last}
        </ModalHeader>
        <ModalBody>
          <div className={Styles.infoItems}>
            <div className={Styles.labels}>
              <p>Nationality: </p>
              <p>Gender: </p>
              <p>Day of Birth: </p>
              <p>Email: </p>
              <p>Phone: </p>
            </div>
            <div className={Styles.info}>
              <p id="nat">{props.selectedUser.nat}</p>
              <p id="gender">{props.selectedUser.gender}</p>
              <p id="dob">
                {birth_date}, {props.selectedUser.dob?.age} years
              </p>
              <p id="email">{props.selectedUser.email}</p>
              <p id="phone">
                <img src="/contact_phone_black_24dp.svg" alt="phone" />{" "}
                {props.selectedUser.phone} / {props.selectedUser.cell}
              </p>
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="success" onClick={callUser}>
            Call
          </Button>{" "}
          <Button color="secondary" onClick={props.toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default UserModal;
