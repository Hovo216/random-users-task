import { RECEIVE_USERS, SET_IS_FETCHING } from "./constants";

const initState = {
  users: [],
  is_fetching: true,
};

export default (state = initState, action) => {
  switch (action.type) {
    case RECEIVE_USERS: {
      return {
        ...state,
        users: action.users,
      };
    }
    case SET_IS_FETCHING: {
      return {
        ...state,
        is_fetching: action.bool,
      };
    }
    default:
      return state;
  }
};
