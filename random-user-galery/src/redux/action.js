import { RECEIVE_USERS, SET_IS_FETCHING } from "./constants";

import * as API from "./API";

export const receiveUsers = (users) => ({
  type: RECEIVE_USERS,
  users,
});

export const setIsFetching = (bool) => ({
  type: SET_IS_FETCHING,
  bool,
});

export const receive_users = (page) => async (dispatch) => {
  try {
    dispatch(setIsFetching(true));
    const response = await API.receiveUsers(page);
    const data = await response.json();
    dispatch(receiveUsers(data.results));
    dispatch(setIsFetching(false));
  } catch (err) {
    alert("Can't receive users. Pleace reload page.");
    console.log(err);
  }
};
